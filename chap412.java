package conditionFiles;

/**
 * @author jjpar
 * 2부터 시작되는 구구단 소스를 작성
 * 단, 곱하는 수는 1, 2, 3까지만 곱하는 형식
 * ex) 2*1 = 2, 2*2 = 4, 2*3 = 6 까지의 형태로 결과 출력
 */
public class chap412 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int result = 0;
		for(int i = 2; i <= 9; i++) {
			for(int j = 1; j <= 3; j++) {
				result = i * j;
				System.out.println(i + "*" + j + "=" + result);
			}
			System.out.println();
		}
	}

}
