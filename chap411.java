package conditionFiles;

/**
 * @author jjpar
 *	피보나치 수열은 앞의 두 수를 더해서 다음 수를 만들어 나가는 수열이다.
 *	1부터 시작하는 피보나치 수열의 10번쨰 수는 무엇인지 계산
 */
public class chap411 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 피보나치 수열의 시작의 첫 두 숫자를 1, 1로 한다.
		int num3 = 0; // 세번째 값
		int[] arrayNum = new int[10];
		
		arrayNum[0] = 1;
		arrayNum[1] = 1;
		
		System.out.print(arrayNum[0] + ", " + arrayNum[1] + ", ");
		for(int i = 2; i < arrayNum.length; i++) {
			arrayNum[i] = arrayNum[i-2] + arrayNum[i-1];
			num3 = arrayNum[i];
			System.out.print(num3 + ", ");
		}
	}

}
