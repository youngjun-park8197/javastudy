package conditionFiles;

/*
 * 아래의 이중 for문을 while문으로 바꾸는 문제
 * 해결 불가
 */

public class chap45 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*for(int i = 0; i <= 10; i++) {
			for(int j = 0; j <= i; j++) {
				System.out.println("*");
			}
			System.out.println();
		}*/
		int i = 0;
		while(i <= 10) {
			int j = 0;
			while(j<=i) {
				j++;
			}
			System.out.println("*");
			i++;
		}
	}
}
