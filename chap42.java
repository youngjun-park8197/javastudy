
// Chapter 4. 
// 4-2
// 1 - 20까지의 정수 중 2 또는 3의 배수가 아닌 수의 총합을 구하시오.

package conditionFiles;

public class chap42 {
	public static void main(String args[]) {
		int sum = 0;
		for(int num = 1; num < 20; num++) {
			if(num%2 != 0 && num%3 != 0) {
				System.out.println("2의 배수, 3의 배수가 아닌 수는 " + num + " 입니다.");
				sum += num;
			}
		}
	}
}
