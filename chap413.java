package conditionFiles;

/**
 * @author jjpar
 *	주어진 문자열 value가 숫자인지를 판별하는 프로그램
 *	알맞은 코드를 작성하여 완성
 */
public class chap413 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String value = "12o34";
		boolean isNumber = true;
		
		// 반복문과 charAt(int i)를 이용해서 문자열의 문자를 하나씩 읽어서 검사
		for(int i = 0; i < value.length(); i++) {
			// 코드 작성
			char check = value.charAt(i);
			if(check < 48 || check > 58) {
//				System.out.println(check);
				isNumber = false;
			} else {
				System.out.print(check);
				isNumber = true;
			}
			System.out.println();
		}
		
		if(isNumber) {
			System.out.println(value + "는 숫자입니다. ");
		} else {
			System.out.println(value + "는 숫자가 아닙니다. ");
		}
	}

}
