package conditionFiles;

/**
 * @author jjpar
 *	numbers between 1 and 100, repeatedly check numbers and 
 *	if your number is same as computers', then it'll be finished.
 */
public class chapr414 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double randomNum = Math.random();  //랜덤 변수값 1 ~ 100 사이의 수를 입력하도록 하기 (기억)
		int answer = (int)(randomNum * 100) + 1;
		int input = 0;
		int count = 0;
		
		java.util.Scanner s = new java.util.Scanner(System.in);
		
		do {
			count++;
			System.out.print("please type numbers between 1 and 100 : ");
			input = s.nextInt();
			
			System.out.printf("check this number(computers' number) : %d ", answer);
			
			if(input < answer) {
				System.out.println("put again your higher number than previous one");
			}
			else if(input > answer) {
				System.out.println("put again your lower number than previous one");
			}
			
		} while(input!=answer);  // while문의 조건이 true일때까지 반복해서 수행
		System.out.println("That's it, you've got same number as computers'");
		System.out.printf("you've tried %d times", count);
	}

}
