package conditionFiles;

// chapter 4-3
// 1+ 1+2 + 1+2+3 + ... + 1+2+3+...+10 의 결과 출력

public class chap43 {
	public static void main(String args[]) {
		int sum = 0;
		for(int i = 1; i <= 10; i++) {
			System.out.println("i값은 = " + i);
			for(int j = 1; j <= i; j++) {
				sum += j;
				System.out.println("j값은 = " + j);
			}
		}
		System.out.println("더한 값은 = " + sum);
	}
}